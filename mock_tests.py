from typing import Tuple
import unittest
class TestStringMethods(unittest.TestCase):


    def get_available_currencies(self):
        available_currencies = [{'id': 'SEK', 'description': 'SEK Sweden, kronor'},
                                {'id': 'ATS', 'description': 'ATS Austria, shilling'},
                                {'id': 'AUD', 'description': 'AUD Australian, dollar'},
                                {'id': 'BEF', 'description': 'BEF Belgien, franc'},
                                {'id': 'BRL', 'description': 'BRL Brazilien, real'},
                                {'id': 'CAD', 'description': 'CAD Canada, dollar'},
                                {'id': 'CHF', 'description': 'CHF Switzerland, francs'},
                                {'id': 'CNY', 'description': 'CNY China, yuan renminbi'},
                                {'id': 'CYP', 'description': 'CYP Cyprus, pound'},
                                {'id': 'CZK', 'description': 'CZK Czech Republic, koruna'},
                                {'id': 'USD', 'description': 'USD US, dollar'}]
        return available_currencies


    def test_currency_list_in_schema(self):
        is_list = True
        is_currency_dict = True
        is_id_str = True
        is_description_str = True
        
        available_currencies = self.get_available_currencies()
        
        if len(available_currencies) > 0:
            is_list = True
            for currency in available_currencies:
                if isinstance(currency, dict):
                    if not isinstance(currency['id'], str) or len(currency['id']) == 0:
                        is_id_str = False
                        break
                    if not isinstance(currency['description'], str) or len(currency['description']) == 0:
                        is_description_str = False
                        break
                else:
                    is_currency_dict = False
                    break
        else: 
            is_list = False

        self.assertTrue(is_list, "The available currencies list is empty")
        self.assertTrue(is_currency_dict, "The currency is not a dictionary")
        self.assertTrue(is_id_str, 
                        "The currency {} id is not a string".format(currency))
        self.assertTrue(is_description_str, 
                        "The currency {} description is not a string".format(currency))


    def test_known_currencies(self):
        """
        test the existence of 3 known keys in the currencies list 
        """
        is_usd_present = False
        is_sek_present = False
        is_cad_present = False
        
        available_currencies = self.get_available_currencies()

        for currency in available_currencies:
            if currency['id'] == 'USD':
                is_usd_present = True
            if currency['id'] == 'SEK':
                is_sek_present = True
            if currency['id'] == 'CAD':
                is_cad_present = True
            if is_usd_present and is_sek_present and is_cad_present:
                break
        
        self.assertTrue(is_cad_present and is_sek_present and is_usd_present, "Failed to find currency in list")

if __name__ == '__main__':
    unittest.main()
from ssl import VERIFY_X509_TRUSTED_FIRST
from typing import Tuple
import unittest
import http.client
class TestStringMethods(unittest.TestCase):


    def get_available_currencies():
        conn = http.client.HTTPSConnection("currencyconverter.p.rapidapi.com")

        headers = {
            'x-rapidapi-key': "5268ab3d00msh1c758a44851a6f8p1894eajsn9eaff19703da",
            'x-rapidapi-host': "currencyconverter.p.rapidapi.com"
            }
        
        conn.request("GET", "/availablecurrencies", headers=headers)
        response = conn.getresponse()
        available_currencies = response.read()

        conn.close()
        return available_currencies


    def test_currency_list_in_schema(self):
        is_list = True
        is_currency_dict = True
        is_id_str = True
        is_description_str = True
        
        available_currencies = self.get_available_currencies()
        
        if len(available_currencies) > 0:
            is_list = True
            for currency in available_currencies:
                if isinstance(currency, dict):
                    if not isinstance(currency['id'], str) or len(currency['id']) == 0:
                        is_id_str = False
                        break
                    if not isinstance(currency['description'], str) or len(currency['description']) == 0:
                        is_description_str = False
                        break
                else:
                    is_currency_dict = False
                    break
        else: 
            is_list = False

        self.assertTrue(is_list, "The available currencies list is empty")
        self.assertTrue(is_currency_dict, "The currency is not a dictionary")
        self.assertTrue(is_id_str, 
                        "The currency {} id is not a string".format(currency))
        self.assertTrue(is_description_str, 
                        "The currency {} description is not a string".format(currency))


    def test_known_currencies(self):
        """
        test the existence of 3 known keys in the currencies list 
        """
        is_usd_present = False
        is_sek_present = False
        is_cad_present = False
        
        available_currencies = self.get_available_currencies()

        for currency in available_currencies:
            if currency['id'] == 'USD':
                is_usd_present = True
            if currency['id'] == 'SEK':
                is_sek_present = True
            if currency['id'] == 'CAD':
                is_cad_present = True
            if is_usd_present and is_sek_present and is_cad_present:
                break
        
        self.assertTrue(is_cad_present and is_sek_present and is_usd_present, "Failed to find currency in list")


    def test_convert_currencies(self):
        is_from_str = True
        is_to_str = True
        is_to_amount_int = True
        
        conn = http.client.HTTPSConnection("currencyconverter.p.rapidapi.com")

        headers = {
            'x-rapidapi-key': "5268ab3d00msh1c758a44851a6f8p1894eajsn9eaff19703da",
            'x-rapidapi-host': "currencyconverter.p.rapidapi.com"
            }
        available_currencies = self.get_available_currencies()
        
        for from_currency in available_currencies:
            for to_currency in available_currencies.reverse():
                conn.request("GET", "/?from_amount=1&from={}&to={}".format(from_currency, to_currency), 
                            headers=headers)
                res = conn.getresponse()
                data = res.read()

                if not isinstance(data['from'], str):
                    is_from_str = False
                    break

                if not isinstance(data['to'], str):
                    is_to_str = False
                    break

                if not isinstance(data['to_amount'], int):
                    is_to_amount_int = False
                    break
            if not (is_from_str and is_to_str and is_to_amount_int):
                break
        
        conn.close()
        self.assertTrue(is_from_str, 
                        "The from field in the response is not a string in {}".format(data))
        self.assertTrue(is_to_str, 
                        "The to field in the response is not a string in {}".format(data))
        self.assertTrue(is_to_amount_int, 
                        "The to amount field in the response is not an int in {}".format(data))


    def test_request_without_headers(self):
        conn = http.client.HTTPSConnection("currencyconverter.p.rapidapi.com")
        conn.port=443
        
        conn.request("GET", "/?from_amount=1&from=USD&to=SEK")

        res = conn.getresponse()
        self.assertEqual(res.status, "401")
        conn.close()


    def test_bad_requests(self):
        conn = http.client.HTTPSConnection("currencyconverter.p.rapidapi.com")
        conn.port=443
        headers = {
            'x-rapidapi-key': "5268ab3d00msh1c758a44851a6f8p1894eajsn9eaff19703da",
            'x-rapidapi-host': "currencyconverter.p.rapidapi.com"
            }

        conn.request("GET", "/?from_amount=-1&from=USD&to=SEK", headers=headers)

        bad_amount = conn.getresponse()
        self.assertEqual(bad_amount.status, "400")
        
        conn.request("GET", "/?from_amount=1&from=gruffalo&to=SEK", headers=headers)
        bad_from = conn.getresponse()
        self.assertEqual(bad_from.status, "406")

        conn.request("GET", "/?from_amount=1&from=USD&to=whale", headers=headers)
        bad_to = conn.getresponse()
        self.assertEqual(bad_to.status, "406")

        conn.close()

if __name__ == '__main__':
    unittest.main()